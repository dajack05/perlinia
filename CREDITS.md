## Concept
- Jonah did it first... There, it's official.

## Perlin.ts
- Originally written in JS by https://joeiddon.github.io/projects/javascript/perlin.html
- I converted it to TS
- I concerted to use Alea (PRNG)

## alea.ts
- Originally from https://github.com/davidbau/seedrandom/tree/released/lib
- I modified it by converting it to TS

## Graphics
- All are taken without shame from opengameart.org