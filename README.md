# Perlinia
Perlinia is a basic procedurally generated 2D voxel engine. What you choose to do with it, I don't care.

Perlinia is written entirely in TypeScript and requires no external dependencies. All libraries are included in /src/lib/ though I'm slowing attempting my own versions of those.

Check out CREDTS.md for credits.

## v0.3 Goals
- [ ] Scriptable world generation
- [ ] Scriptable blocks
- [ ] Unified physics (maybe Box2d or something?)

## v0.2 Goals
- [x] Refactor Vec lib into homebrew version
- [ ] Create global state machine
- [ ] Simplify coordinate system
- [ ] Refactor chunk/tree system

## v0.1 Roadmap/Goals
- [x] Generate chunks based on seed
- [x] Allow infinite movement
- [x] Handle basic collision detection
- [ ] Work on desktop and mobile
  - [x] Mouse + Keyboard Controls
  - [x] Touch Controls
  - [ ] Dynamic viewport size
- [ ] Save/Load worlds