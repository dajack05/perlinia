import Vec2 from "./lib/Vec2";

export class Input {
    private static DownKeys: { [index: string]: { value: boolean } } = {};

    private static DownBtns: boolean[] = [];
    private static MousePos = new Vec2(0, 0);
    private static MouseDelta = new Vec2(0, 0);

    private static leftBtn = document.getElementById('leftBtn') as HTMLButtonElement;
    private static rightBtn = document.getElementById('rightBtn') as HTMLButtonElement;
    private static jumpBtn = document.getElementById('jumpBtn') as HTMLButtonElement;
    private static editBtn = document.getElementById('editBtn') as HTMLButtonElement;

    static editMode = false;
    static addMode = false;

    static Init(canvas: HTMLCanvasElement) {
        document.addEventListener('keydown', (evt) => Input.DownKeys[evt.key] = { value: true });
        document.addEventListener('keyup', (evt) => Input.DownKeys[evt.key] = { value: false });
        document.addEventListener('mousemove', (evt) => {
            Input.MousePos = new Vec2(
                evt.x / canvas.clientWidth * canvas.width,
                evt.y / canvas.clientWidth * canvas.width
            );

            Input.MouseDelta = new Vec2(
                evt.movementX,
                evt.movementY
            );
        })
        document.addEventListener('mousedown', (evt) => Input.DownBtns[evt.button] = true);
        document.addEventListener('mouseup', (evt) => Input.DownBtns[evt.button] = false);

        document.addEventListener('touchstart', (evt) => Input.CheckTouch(evt, canvas));
        document.addEventListener('touchend', (evt) => Input.CheckTouch(evt, canvas));
        document.addEventListener('touchmove', (evt) => Input.CheckTouch(evt, canvas));

        Input.leftBtn.style.visibility = "hidden";
        Input.rightBtn.style.visibility = "hidden";
        Input.jumpBtn.style.visibility = "hidden";
        Input.editBtn.style.visibility = "hidden";
    }

    private static CheckTouch(evt: TouchEvent, canvas: HTMLCanvasElement) {
        let targets: Element[] = [];

        for (let i = 0; i < evt.touches.length; i++) {
            const touch = evt.touches[i];
            const element = document.elementFromPoint(touch.clientX, touch.clientY);
            targets.push(element);
        }

        Input.leftBtn.style.visibility = "visible";
        Input.rightBtn.style.visibility = "visible";
        Input.jumpBtn.style.visibility = Input.editMode ? "hidden" : "visible";
        Input.editBtn.style.visibility = "visible";

        Input.leftBtn.innerText = Input.editMode ? "Add Block" : "Left";
        Input.rightBtn.innerText = Input.editMode ? "Remove Block" : "Right";

        if (evt.touches.length > 0) {
            this.MousePos = new Vec2(
                evt.touches[0].clientX / canvas.clientWidth * canvas.width,
                evt.touches[0].clientY / canvas.clientWidth * canvas.width
            );
        }

        if (Input.editMode) {
            Input.leftBtn.style.backgroundColor = Input.addMode ? "#FFFFFF" : "#222222";
            Input.rightBtn.style.backgroundColor = !Input.addMode ? "#FFFFFF" : "#222222";

            if (evt.type == 'touchstart') Input.DownBtns[0] = true;
            else if (evt.type == 'touchend') Input.DownBtns[0] = false;

            if (targets.indexOf(Input.leftBtn) >= 0) {
                Input.addMode = true;
            }
            if (targets.indexOf(Input.rightBtn) >= 0) {
                Input.addMode = false;
            }

        } else {
            Input.leftBtn.style.backgroundColor =   "rgba(255,255,255,0.5)";
            Input.rightBtn.style.backgroundColor =  "rgba(255,255,255,0.5)";
            Input.DownKeys[' '] = { value: targets.indexOf(Input.jumpBtn) >= 0 };
            Input.DownKeys['d'] = { value: targets.indexOf(Input.rightBtn) >= 0 };
            Input.DownKeys['a'] = { value: targets.indexOf(Input.leftBtn) >= 0 };
        }

        if (targets.indexOf(Input.editBtn) >= 0) {
            Input.editMode = !Input.editMode;
        }
    }

    static IsKeyDown(key: string): boolean {
        return Input.DownKeys[key] && Input.DownKeys[key].value;
    }

    static IsBtnDown(btn: number): boolean {
        return Input.DownBtns[btn] && Input.DownBtns[btn];
    }

    static GetMousePos(): Vec2 {
        return Input.MousePos;
    }
}