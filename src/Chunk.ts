import Perlin from "./lib/Perlin";
import Vec2 from "./lib/Vec2";
import { Tileset } from "./Tileset";

export enum BlockType {
    AIR,
    DIRT,
    ROCK,
    COAL,
    SAND,
    GOLD,
    COUNT
}

const chunkSize = 20;
export class Chunk {

    public static chunkSize = new Vec2(chunkSize, chunkSize);
    public static scale = 0.1;

    public debug = false;

    data: BlockType[] = [];

    offset = new Vec2();

    constructor(perlin: Perlin = null, offset: Vec2 = null, debug = false) {
        this.debug = debug;

        if (perlin)
            this.gen(offset, perlin);
    }

    public gen(offset: Vec2, perlin: Perlin) {
        this.offset = offset;

        //Dirt/Rock/Coal
        for (let i = 0; i < Chunk.chunkSize.x * Chunk.chunkSize.y; i++) {
            const x = i % Chunk.chunkSize.x;
            const y = i / Chunk.chunkSize.x;

            //Dirt/Rock
            let p = perlin.get(123 + this.offset.x + x * Chunk.scale, 123 + this.offset.y + y * Chunk.scale);
            this.data[i] = p < 0 ? BlockType.ROCK : BlockType.DIRT;

            //Coal
            p = perlin.get(234 + this.offset.x + x * Chunk.scale, 234 + this.offset.y + y * Chunk.scale);
            this.data[i] = p > 0.45 ? BlockType.COAL : this.data[i];

            //Caves
            p = perlin.get(321 + this.offset.x + x * Chunk.scale, 321 + this.offset.y + y * Chunk.scale);
            this.data[i] = p > 0.2 ? BlockType.AIR : this.data[i];

            //Gold
            p = perlin.get(589 + this.offset.x + x * Chunk.scale, 124 + this.offset.y + y * Chunk.scale);
            this.data[i] = p > 0.5 ? BlockType.GOLD : this.data[i];
        }

        // Ground/Sky
        if (this.offset.y == 0) {
            for (let i = 0; i < Chunk.chunkSize.x; i++) {
                const p = perlin.get(345 + this.offset.x + i * Chunk.scale, 345) / 2 + 0.5;
                for (let j = this.offset.y; j < p * Chunk.chunkSize.x; j++) {
                    this.data[i + j * Chunk.chunkSize.x] = BlockType.AIR;
                }
            }
        }
    }

    public at(location: Vec2): BlockType {
        const idx = location.x + location.y * Chunk.chunkSize.x;
        if (idx < this.data.length && idx >= 0)
            return this.data[idx];

        return BlockType.AIR;
    }

    public set(location: Vec2, block: BlockType) {
        let loc = location.floor();
        let idx = loc.x + loc.y * Chunk.chunkSize.x;
        if (idx >= 0 && idx < this.data.length) {
            this.data[idx] = block;
        }
    }

    public draw(ctx: CanvasRenderingContext2D, tileset: Tileset) {
        for (let i = 0; i < this.data.length; i++) {
            const x = i % Chunk.chunkSize.x;
            const y = Math.floor(i / Chunk.chunkSize.x);

            const tile = this.data[i];
            const src = tileset.getRect(tile);

            //Should we show grass?
            const tileAbove = this.data[x + (y - 1) * Chunk.chunkSize.x];
            if (tileAbove == BlockType.AIR && tile !== BlockType.GOLD) {
                ctx.drawImage(tileset.image,
                    src.x, src.y + 32, src.w, src.h,
                    x * Tileset.tileSize.x, y * Tileset.tileSize.y,
                    Tileset.tileSize.x, Tileset.tileSize.y);
            } else {
                ctx.drawImage(tileset.image,
                    src.x, src.y, src.w, src.h,
                    x * Tileset.tileSize.x, y * Tileset.tileSize.y,
                    Tileset.tileSize.x, Tileset.tileSize.y);
            }
        }

        if (this.debug) {
            //Draw chunk bounds
            ctx.strokeStyle = "#FF0000";
            ctx.strokeRect(
                0, 0,
                Chunk.chunkSize.x * Tileset.tileSize.x, Chunk.chunkSize.y * Tileset.tileSize.y);
        }
    }

    public write(): string {
        return JSON.stringify(this.data);
    }
}