import { BlockType, Chunk } from "./Chunk";
import Perlin from "./lib/Perlin";
import { LoadPackage } from "./SaveLoader";
import { Tileset } from "./Tileset";
import Vec2 from "./lib/Vec2";

interface CellDefList { [index: string]: { cell: Chunk } };

export enum Direction {
    N,
    S,
    E,
    W,

    NE,
    SE,
    SW,
    NW,
}

const Offsets = [
    new Vec2(0, -1),
    new Vec2(0, 1),
    new Vec2(1, 0),
    new Vec2(-1, 0),

    new Vec2(1, -1),
    new Vec2(1, 1),
    new Vec2(-1, 1),
    new Vec2(-1, -1),
];

export class ChunkTree {
    static drawDist = 1.5;

    noise: Perlin;
    cellList: string[] = [];
    cells: CellDefList = {};
    target = new Vec2();

    seed: number;
    debug: boolean;

    constructor(seed = Math.random()*Number.MAX_VALUE, debug = false) {
        this.debug = debug;
        this.seed = seed;
        this.noise = new Perlin(this.seed);

        this.cells[new Vec2(0, 0).asString()] = { cell: new Chunk(this.noise, new Vec2(0, 0), this.debug) };
        this.cellList.push(new Vec2(0, 0).asString());
    }

    public moveFocus(direction: Direction) {
        this.target = this.target.add(Offsets[direction]);
    }

    public add(direction: Direction) {
        if (!this.cells[this.target.add(Offsets[direction]).asString()]) {
            let offset = this.cells[this.target.asString()].cell.offset;
            offset = offset.add(Offsets[direction].multiply(Chunk.chunkSize.multiplyScalar(Chunk.scale)));

            const chunk = new Chunk(this.noise, offset, this.debug);

            this.cells[this.target.add(Offsets[direction]).asString()] = { cell: chunk };
            this.cellList.push(this.target.add(Offsets[direction]).asString());
        }
    }

    public draw(ctx: CanvasRenderingContext2D, tileset: Tileset) {
        for (const idx of this.cellList) {
            const chunk = this.cells[idx].cell;
            const location = new Vec2(parseInt(idx.split(',')[0]), parseInt(idx.split(',')[1]));

            //Cull Chunks
            if (location.distanceTo(this.target) <= ChunkTree.drawDist) {
                // {
                const x = -(this.target.x * Chunk.chunkSize.x * Tileset.tileSize.x) + (location.x * Chunk.chunkSize.x * Tileset.tileSize.x);
                const y = -(this.target.y * Chunk.chunkSize.y * Tileset.tileSize.y) + (location.y * Chunk.chunkSize.y * Tileset.tileSize.y);

                ctx.translate(x, y);
                chunk.draw(ctx, tileset);

                ctx.translate(-x, -y);
            }
        }
    }

    public getBlockAt(location: Vec2): BlockType {
        const chunkblock = this.locationToChunkBlock(location);

        if (chunkblock) {
            const chunk = this.cells[chunkblock.chunkIdx.asString()].cell;
            const b = chunk.at(chunkblock.blockIdx);
            return b;
        }

        return BlockType.AIR;
    }

    public setBlockAt(location: Vec2, block: BlockType) {
        const chunkblock = this.locationToChunkBlock(location, true);
        if (chunkblock) {
            this.cells[chunkblock.chunkIdx.asString()].cell.set(chunkblock.blockIdx, block);
        }
    }

    public set(data: LoadPackage) {
        this.cellList = data.cellList;
        this.cells = data.cells;
        this.seed = data.seed;

        Chunk.chunkSize = data.chunk_size;
        Chunk.scale = data.scale;

        this.noise = new Perlin(this.seed);
    }

    public length(): number {
        return this.cellList.length;
    }

    locationToChunkBlock(location: Vec2, print = false): { blockLocation: Vec2, chunkIdx: Vec2, blockIdx: Vec2 } | null {
        //Figure out which chunk we're looking into
        const blockLocation = location
            .divide(Tileset.tileSize)
            .floor();

        const chunkLocation = blockLocation
            .divide(Chunk.chunkSize)
            .floor();

        const chunkIndex = chunkLocation.add(this.target);  //World space

        const fixedBockLocation = blockLocation
            .subtract(chunkLocation.multiply(Chunk.chunkSize));

        const exists = this.cells[chunkIndex.asString()] ? true : false;
        if (!exists) {
            return null;
        }

        return {
            blockLocation: blockLocation,
            blockIdx: fixedBockLocation,
            chunkIdx: chunkIndex
        };
    }
}