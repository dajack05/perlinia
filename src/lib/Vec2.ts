/**
 * @class Vec2
 * 
 * @classdesc Vec2 impliments all basic Vector math operations in a semi-static manor. All methods return a NEW Vec2, and do NOT affect the original Vec2.
 */
export default class Vec2 {
    readonly x: number;
    readonly y: number;

    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y
    }

    add(v2: Vec2): Vec2 {
        return new Vec2(this.x + v2.x, this.y + v2.y);
    }

    subtract(v2: Vec2): Vec2 {
        return new Vec2(this.x - v2.x, this.y - v2.y);
    }

    multiply(v2: Vec2): Vec2 {
        return new Vec2(this.x * v2.x, this.y * v2.y);
    }

    divide(v2: Vec2): Vec2 {
        return new Vec2(this.x / v2.x, this.y / v2.y);
    }

    multiplyScalar(value: number): Vec2 {
        return this.multiply(new Vec2(value, value));
    }

    divideScalar(value: number): Vec2 {
        return this.divide(new Vec2(value, value));
    }

    length(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    distanceTo(point: Vec2): number {
        return this.subtract(point).length();
    }

    normalized(): Vec2 {
        return this.divideScalar(this.length());
    }

    floor(): Vec2 {
        return new Vec2(Math.floor(this.x), Math.floor(this.y));
    }

    clone(): Vec2 {
        return new Vec2(this.x, this.y);
    }

    lerp(toward: Vec2, t: number): Vec2 {
        const ax = this.x;
        const ay = this.y;
        return new Vec2(ax + t * (toward.x - ax), ay + t * (toward.y - ay));
    }

    //TEMP!!!
    asString(): string {
        return `${this.x},${this.y}`;
    }
}