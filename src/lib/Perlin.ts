import { Alea } from "./Alea";
import Vec2 from "./Vec2";

export default class Perlin {
    seed: number;
    gradients: { [index: string]: { message: Vec2 } } = {};
    memory: { [index: string]: { message: number } } = {};
    rng: Alea;

    gradientsLog: string[] = [];

    constructor(seed = -1) {
        this.seed = seed;
        if (this.seed < 0)
            this.seed = Math.random();
        this.rng = new Alea(this.seed);
    }

    private rand_vect(): Vec2 {
        let theta = this.rng.next() * 2 * Math.PI;
        return new Vec2(Math.cos(theta), Math.sin(theta));
    }

    private dot_prod_grid(x: number, y: number, vx: number, vy: number): number {
        let g_vect: Vec2;
        let d_vect = new Vec2(x - vx, y - vy);
        let idx = `${vx},${vy}`;
        if (this.gradients[idx]) {
            g_vect = this.gradients[idx].message;
        } else {
            g_vect = this.rand_vect();
            this.gradients[idx] = { message: g_vect };
            this.gradientsLog.push(idx);
        }
        return d_vect.x * g_vect.x + d_vect.y * g_vect.y;
    }

    private smootherstep(x: number): number {
        return 6 * x ** 5 - 15 * x ** 4 + 10 * x ** 3;
    }

    private interp(x: number, a: number, b: number): number {
        return a + this.smootherstep(x) * (b - a);
    }

    reseed() {
        this.gradients = {};
        this.gradientsLog = [];
        this.memory = {};
    }

    get(x: number, y: number): number {
        let idx = `${x},${y}`;
        if (this.memory[idx])
            return this.memory[idx].message;
        let xf = Math.floor(x);
        let yf = Math.floor(y);
        //interpolate
        let tl = this.dot_prod_grid(x, y, xf, yf);
        let tr = this.dot_prod_grid(x, y, xf + 1, yf);
        let bl = this.dot_prod_grid(x, y, xf, yf + 1);
        let br = this.dot_prod_grid(x, y, xf + 1, yf + 1);
        let xt = this.interp(x - xf, tl, tr);
        let xb = this.interp(x - xf, bl, br);
        let v = this.interp(y - yf, xt, xb);
        this.memory[idx] = { message: v };
        return v;
    }
}