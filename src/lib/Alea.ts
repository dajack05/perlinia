export class Alea {
    c: number;
    s0: number;
    s1: number;
    s2: number;

    constructor(seed: any) {
        // Apply the seeding algorithm from Baagoe.
        this.c = 1;
        this.s0 = Mash(' ');
        this.s1 = Mash(' ');
        this.s2 = Mash(' ');
        this.s0 -= Mash(seed);
        if (this.s0 < 0) { this.s0 += 1; }
        this.s1 -= Mash(seed);
        if (this.s1 < 0) { this.s1 += 1; }
        this.s2 -= Mash(seed);
        if (this.s2 < 0) { this.s2 += 1; }
    }

    next() {
        var t = 2091639 * this.s0 + this.c * 2.3283064365386963e-10; // 2^-32
        this.s0 = this.s1;
        this.s1 = this.s2;
        return this.s2 = t - (this.c = t | 0);
    }
}

function Mash(data: any) {
    var n = 0xefc8249d;

    data = String(data);
    for (var i = 0; i < data.length; i++) {
        n += data.charCodeAt(i);
        var h = 0.02519603282416938 * n;
        n = h >>> 0;
        h -= n;
        h *= n;
        n = h >>> 0;
        h -= n;
        n += h * 0x100000000; // 2^32
    }
    return (n >>> 0) * 2.3283064365386963e-10; // 2^-32
}