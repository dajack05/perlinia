import Vec2 from "./lib/Vec2";
import * as bg0 from "./res/bg/parallax-mountain-bg.png";
import * as bg1 from "./res/bg/parallax-mountain-montain-far.png";
import * as bg2 from "./res/bg/parallax-mountain-mountains.png";
import * as bg3 from "./res/bg/parallax-mountain-trees.png";

let ready = 0;

const images = [
    new Image(),
    new Image(),
    new Image(),
    new Image(),
];

let SCALES = [
    1, 1, 1, 1,
];
for (const img of images) {
    img.onload = () => {
        SCALES[ready] = 480 / img.height;
        ready++;
    };
}

images[0].src = bg0.default;
images[1].src = bg1.default;
images[2].src = bg2.default;
images[3].src = bg3.default;

function DrawBG(ctx: CanvasRenderingContext2D, offset: Vec2) {
    if (ready >= images.length) {
        for (let i = 0; i < images.length; i++) {
            const image = images[i];

            const IW = image.width * SCALES[i];
            const IH = image.height * SCALES[i];

            let distort = offset
                .divideScalar(8)
                .multiplyScalar(i);

            const remaining = distort.x % IW;
            distort = new Vec2(remaining, 0);

            ctx.drawImage(image,
                -distort.x - IW, -distort.y,
                IW, IH);
            ctx.drawImage(image,
                -distort.x, -distort.y,
                IW, IH);
            ctx.drawImage(image,
                -distort.x + IW, -distort.y,
                IW, IH);
        }
    }
}

export default DrawBG;