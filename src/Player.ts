import { Chunk } from "./Chunk";
import { ChunkTree } from "./ChunkTree";
import { Input } from "./Input";
import { Collider, PhysicsWorld } from "./Physics";
import { Tileset } from "./Tileset";

import * as georgeImg from "./res/george.png";
import Vec2 from "./lib/Vec2";

const COLLIDER_SIZE = new Vec2(1.5, 1.8)
const COLLIDER_OFFSET = COLLIDER_SIZE.divideScalar(2);

const FRAME_SPEED = 0.2;
const FRAMES = [
    [0],            //Idle
    [1, 2, 3, 4]    //Walk
];

export class Player {
    jumpVel = 18;
    speed = 12;
    position = new Vec2();

    grav = new Vec2(0, 4);
    vel = new Vec2(0, 0);

    grounded = false;

    color: string;

    tex = new Image();
    loaded = false;

    anim = 1;
    frame = 0;
    flipped = false;

    constructor() {
        this.tex.onload = () => {
            this.loaded = true;
        };
        this.tex.src = georgeImg.default;
    }

    update(tree: ChunkTree, delta: number) {
        this.vel = this.vel.add(this.grav.multiplyScalar(delta));

        let moveVec = this.vel.multiplyScalar(delta);

        if (Input.IsKeyDown('a')) {
            moveVec = moveVec.subtract(new Vec2(this.speed * delta, 0));
        }
        if (Input.IsKeyDown('d')) {
            moveVec = moveVec.add(new Vec2(this.speed * delta, 0));
        }

        const ycomp = moveVec.multiply(new Vec2(0, 1));
        this.position = this.position.add(ycomp);
        this.grounded = false;
        while (PhysicsWorld.IsSolid(new Collider(this.position.divide(Tileset.tileSize).add(tree.target.multiply(Chunk.chunkSize)).subtract(COLLIDER_OFFSET), COLLIDER_SIZE))) {
            this.position = this.position.subtract(ycomp);
            this.vel = this.vel.multiply(new Vec2(1, 0));
            this.grounded = true;
        }

        this.position = this.position.add(moveVec.multiply(new Vec2(1, 0)));
        if (PhysicsWorld.IsSolid(new Collider(this.position.divide(Tileset.tileSize).add(tree.target.multiply(Chunk.chunkSize)).subtract(COLLIDER_OFFSET), COLLIDER_SIZE))) {
            this.position = this.position.subtract(moveVec.multiply(new Vec2(1, 0)));
            this.vel = this.vel.multiply(new Vec2(0, 1));
        }

        this.flipped = moveVec.x < 0;                   //Moving Left?
        this.anim = Math.abs(moveVec.x) > 0.1 ? 1 : 0;  //Moving at all?

        if (Input.IsKeyDown(' ') && this.grounded) {
            this.vel = new Vec2(0, -this.jumpVel);
        }

        if (this.grounded)
            this.frame += FRAME_SPEED;
        if (this.frame >= FRAMES[this.anim].length || this.frame < FRAMES[this.anim][0]) {
            this.frame = FRAMES[this.anim][0];
        }
    }

    draw(ctx: CanvasRenderingContext2D) {
        //Draw
        if (this.loaded) {
            const x = FRAMES[this.anim][Math.floor(this.frame)] * 32;
            const y = 0;
            const w = 32;
            const h = 32;

            ctx.translate(Math.floor(this.position.x), Math.floor(this.position.y));
            ctx.scale(this.flipped ? -1 : 1, 1);

            ctx.drawImage(this.tex,
                x, y, w, h,
                -16, -16, 32, 32);

            ctx.scale(this.flipped ? -1 : 1, 1);
            ctx.translate(-Math.floor(this.position.x), -Math.floor(this.position.y));
        }
    }
}