import { BlockType } from "./Chunk";
import Vec2 from "./lib/Vec2";
import { Rect } from "./Rect";

export class Tileset {
    ready = false;
    image: HTMLImageElement;
    private cols: number;
    private rows: number;
    private count: number;

    static tileSize = new Vec2(1,1);

    constructor(path: string, cols: number, rows: number) {
        this.image = new Image();
        this.image.onload = () => {
            this.ready = true
            Tileset.tileSize = new Vec2(this.image.width / cols, this.image.height / rows);
        };
        this.image.src = path;

        this.cols = cols;
        this.rows = rows;
        this.count = this.cols * this.rows;
    }

    getRect(block: BlockType): Rect {
        switch (block) {
            case BlockType.DIRT:
                return {
                    x: 4 * Tileset.tileSize.x,
                    y: 1 * Tileset.tileSize.y,
                    w: Tileset.tileSize.x,
                    h: Tileset.tileSize.y
                }
            case BlockType.ROCK:
                return {
                    x: 1 * Tileset.tileSize.x,
                    y: 1 * Tileset.tileSize.y,
                    w: Tileset.tileSize.x,
                    h: Tileset.tileSize.y
                }
            case BlockType.COAL:
                return {
                    x: 7 * Tileset.tileSize.x,
                    y: 1 * Tileset.tileSize.y,
                    w: Tileset.tileSize.x,
                    h: Tileset.tileSize.y
                }
            case BlockType.SAND:
                return {
                    x: 10 * Tileset.tileSize.x,
                    y: 1 * Tileset.tileSize.y,
                    w: Tileset.tileSize.x,
                    h: Tileset.tileSize.y
                }
            case BlockType.GOLD:
                return {
                    x: 13 * Tileset.tileSize.x,
                    y: 6 * Tileset.tileSize.y,
                    w: Tileset.tileSize.x,
                    h: Tileset.tileSize.y
                }
            default:
                return {
                    x: 0,
                    y: 0,
                    w: 0,
                    h: 0
                }
        }
    }
}