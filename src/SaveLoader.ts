import { BlockType, Chunk } from "./Chunk";
import { ChunkTree } from "./ChunkTree";
import Vec2 from "./lib/Vec2";

interface WritableHeader {
    seed: number,
    chunk_size: string,
    scale: number,
    rng_state: number[],
}

interface WritableChunk {
    location: string,
    offset: Vec2,
    data: string,
}

export interface LoadPackage {
    seed: number,
    chunk_size: Vec2,
    scale: number,
    cellList: string[],
    cells: { [index: string]: { cell: Chunk } },
    rng_state: number[],
}

export class SaveLoader {
    static Save(tree: ChunkTree): string {

        console.log(`ChunkSize:${Chunk.chunkSize}`);
        console.log(`Chunk Num:${tree.cellList.length}`);
        let chunks: WritableChunk[] = [];
        for (const idx of tree.cellList) {
            const chunk = tree.cells[idx].cell;
            const writeChunk: WritableChunk = {
                location: idx,
                offset: chunk.offset,
                data: SaveLoader.CompressBlocks(chunk.data)
            };
            chunks.push(writeChunk);
        }

        let locations: string = "";
        let offsets: string = "";
        let datas: string[] = [];
        for (const c of chunks) {
            offsets += SaveLoader.CompressOffset(c.offset as Vec2);
            locations += c.location + "|";
            datas.push(c.data.substr(0, c.data.length - 1));
        }

        offsets = offsets.substr(0, offsets.length - 1);
        locations = locations.substr(0, locations.length - 1);

        /*
        *   Header stuff
        */
        const chunksize = SaveLoader.CompressVec2(Chunk.chunkSize);
        const header: WritableHeader = {
            chunk_size: chunksize.substr(0, chunksize.length - 1),
            scale: Chunk.scale,
            seed: tree.seed,
            rng_state: [
                tree.noise.rng.c,
                tree.noise.rng.s0,
                tree.noise.rng.s1,
                tree.noise.rng.s2,
            ]
        };

        return JSON.stringify({ header, locations, offsets, datas });
    }

    static Load(data: string): LoadPackage {

        const d = JSON.parse(data);

        /*
        *   Header Stuff
        */
        const rawHeader = d.header;
        const chunk_size = SaveLoader.DecompressVec2(rawHeader.chunk_size);
        const scale: number = rawHeader.scale;
        const seed: number = rawHeader.seed;
        const rng_state: number[] = rawHeader.rng_state;

        /*
        *   Location Stuff
        */
        const locationsStrings = (d.locations as string).split("|");
        const locations: Vec2[] = [];
        for (const str of locationsStrings) {
            locations.push(SaveLoader.DecompressLocation(str));
        }

        /*
        *   Offset Stuff
        */
        const offsetsStrings = (d.offsets as string).split("|");
        const offsets: Vec2[] = [];
        for (const str of offsetsStrings) {
            offsets.push(SaveLoader.DecompressOffset(str));
        }

        /*
        *   Data Stuff
        */
        const datasStrings = d.datas as string[];
        const datas: BlockType[][] = [];
        for (const chunkStr of datasStrings) {
            datas.push(SaveLoader.DecompressBlocks(chunkStr));
        }

        /*
        *   Reassemble from arrays
        */
        const cells: { [index: string]: { cell: Chunk } } = {};
        const cellList: string[] = [];

        for (let i = 0; i < offsets.length; i++) {
            const chunk = new Chunk();
            const idx = `${locations[i].x},${locations[i].y}`;

            chunk.offset = offsets[i];
            chunk.data = datas[i];

            cellList.push(idx)
            cells[idx] = { cell: chunk };
        }

        return {
            cellList,
            cells,
            chunk_size,
            scale,
            seed,
            rng_state,
        };
    }

    private static CompressBlocks(data: BlockType[]): string {
        let v = "";

        let count = 0;
        let last = data[0];

        const write = () => {
            v += `${last.toString(16)}${count}|`;
            count = 1;
        };

        for (const block of data) {
            if (block == last) {
                //Duplicate
                count++;
            } else {
                //New
                write();
                last = block;
            }
        }
        write();

        return v;
    }

    private static DecompressBlocks(data: string): BlockType[] {
        const chunkData: BlockType[] = [];
        const parts = data.split("|");
        for (const part of parts) {
            /*
            At this point the format is as follows

            X123 where X is the BlockType and the number is how many follow it
            */
            const type: BlockType = parseInt(part[0], 16);
            const count = parseInt(part.substr(1));
            for (let i = 0; i < count; i++) {
                chunkData.push(type);
            }
        }
        return chunkData;
    }

    private static CompressVec2(Vec2: Vec2, precision?: number): string {
        const xstr = Vec2.x.toString().substring(0, precision);
        const ystr = Vec2.y.toString().substring(0, precision);
        return xstr + "," + ystr + "|";
    }

    private static DecompressVec2(input: string): Vec2 {
        const parts = input.split(",");
        return new Vec2(parseFloat(parts[0]), parseFloat(parts[1]));
    }

    private static CompressOffset(offset: Vec2): string {
        return SaveLoader.CompressVec2(offset);
    }

    private static DecompressOffset(input: string): Vec2 {
        return SaveLoader.DecompressVec2(input);
    }

    private static CompressLocation(location: Vec2): string {
        return SaveLoader.CompressVec2(location);
    }

    private static DecompressLocation(input: string): Vec2 {
        return SaveLoader.DecompressVec2(input);
    }
}