import { BlockType, Chunk } from "./Chunk";
import { ChunkTree, Direction } from "./ChunkTree";
import { Input } from "./Input";
import { Player } from "./Player";
import * as tileimg from "./res/tiles.png";
import { SaveLoader } from "./SaveLoader";
import { Tileset } from "./Tileset";
import { PhysicsWorld } from "./Physics";
import Vec2 from "./lib/Vec2";
import DrawBG from "./Background";

const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const ctx = canvas.getContext('2d');
ctx.imageSmoothingEnabled = false;

const pixelsWide = 640;
canvas.width = pixelsWide;
canvas.height = Math.min(pixelsWide * (window.innerHeight / window.innerWidth), 480);

Input.Init(canvas);

// const saveBtn = document.getElementById('saveBtn') as HTMLButtonElement;
// saveBtn.addEventListener('click', () => {
// 	SaveFile(SaveLoader.Save(tree), "save.json");
// });

// const loadInput = document.getElementById('loadInput') as HTMLInputElement;
// loadInput.addEventListener('change', (evt) => {
// 	const files: FileList = (<HTMLInputElement>evt.target).files;
// 	if (files.length > 0) {
// 		const reader = new FileReader();
// 		reader.addEventListener('load', (evt) => {
// 			tree.set(SaveLoader.Load(evt.target.result as string));
// 		});
// 		reader.readAsText(files[0]);
// 	}
// });

const tileset = new Tileset(tileimg.default, 16, 16);

let viewPos = new Vec2();

const tree = new ChunkTree();
verifyChunks();

const player = new Player();
player.position = new Vec2(50, 50);

//Timing
let lastTime = Date.now();

function loop() {
	let now = Date.now();
	const delta = (now - lastTime) * 0.01;
	lastTime = now;

	const targetLoc = player.position.subtract(new Vec2(canvas.width / 2, canvas.height / 2));
	viewPos = viewPos.lerp(targetLoc, delta);

	//Cls
	DrawBG(ctx, viewPos
		.add(tree.target
			.multiply(Chunk.chunkSize)
			.multiply(Tileset.tileSize)));

	//==========================================================================================================================================================================
	ctx.translate(-Math.floor(viewPos.x), -Math.floor(viewPos.y));
	{
		//Camera-space
		tree.draw(ctx, tileset);

		player.update(tree, delta);
		player.draw(ctx);

		//Draw World
		if (tree.debug)
			PhysicsWorld.DebugDraw(ctx, tree);
	}
	ctx.translate(Math.floor(viewPos.x), Math.floor(viewPos.y));

	//==========================================================================================================================================================================
	//Block Selector
	const currentBlockSelection = Input.GetMousePos()
		.divide(Tileset.tileSize)
		.floor()
		.multiply(Tileset.tileSize)
		.subtract(new Vec2(viewPos.x % 16, viewPos.y % 16));

	ctx.strokeStyle = "#0000ff";
	ctx.strokeRect(currentBlockSelection.x - Tileset.tileSize.x, currentBlockSelection.y - Tileset.tileSize.y, Tileset.tileSize.x, Tileset.tileSize.y);
	//==========================================================================================================================================================================
	//Update

	//Terrain modification
	const currentMouseWorldPos = Input.GetMousePos()
		.subtract(new Vec2(viewPos.x % 16, viewPos.y % 16));
	if (Input.IsKeyDown('e')) {
		tree.setBlockAt(currentMouseWorldPos.add(viewPos).subtract(new Vec2(16, 16)), BlockType.AIR);
	}
	if (Input.IsKeyDown('r')) {
		tree.setBlockAt(currentMouseWorldPos.add(viewPos).subtract(new Vec2(16, 16)), BlockType.DIRT);
	}

	//Mobile terrain mod
	if (Input.editMode) {
		if (Input.IsBtnDown(0)) {
			if (Input.addMode) {
				tree.setBlockAt(currentMouseWorldPos.add(viewPos).subtract(new Vec2(16, 16)), BlockType.DIRT);
			} else {
				tree.setBlockAt(currentMouseWorldPos.add(viewPos).subtract(new Vec2(16, 16)), BlockType.AIR);
			}
		}
	}

	{//Change focus when player moves too far
		const d = Chunk.chunkSize.multiply(Tileset.tileSize);
		if (player.position.x >= d.x || player.position.x < 0) {
			const gtz = player.position.x > 0;
			const dir = gtz ? Direction.E : Direction.W;
			tree.add(dir);
			tree.moveFocus(dir);

			player.position = new Vec2(d.x * (gtz ? 0 : 1), player.position.y);
			viewPos = viewPos.add(new Vec2(d.x * (gtz ? -1 : 1), 0))
			verifyChunks();
		}

		if (player.position.y >= d.y || player.position.y < 0) {
			const gtz = player.position.y > 0;
			const dir = gtz ? Direction.S : Direction.N;
			tree.add(dir);
			tree.moveFocus(dir);

			player.position = new Vec2(player.position.x, d.y * (gtz ? 0 : 1));
			viewPos = viewPos.add(new Vec2(0, d.y * (gtz ? -1 : 1)))
			verifyChunks();
		}
	}

	PhysicsWorld.ClearAll();
	PhysicsWorld.Gen(tree, player.position);

	window.requestAnimationFrame(loop.bind(this));
}

function verifyChunks() {
	tree.add(Direction.N);
	tree.add(Direction.S);
	tree.add(Direction.E);
	tree.add(Direction.W);

	tree.add(Direction.NE);
	tree.add(Direction.SE);
	tree.add(Direction.SW);
	tree.add(Direction.NW);
}

function SaveFile(data: string, filename: string) {
	var element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
	element.setAttribute('download', filename);

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
}

window.setTimeout(loop.bind(this), 50);