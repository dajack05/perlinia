import { BlockType, Chunk } from "./Chunk";
import { ChunkTree, Direction } from "./ChunkTree";
import Vec2 from "./lib/Vec2";
import { Tileset } from "./Tileset";

export class Collider {
    exists: boolean;
    origin: Vec2;
    size: Vec2;

    constructor(origin: Vec2, size: Vec2, exists = true) {
        this.origin = origin;
        this.size = size;
        this.exists = exists;
    }

    overlaps(other: Collider): boolean {
        const isInX = this.origin.x < other.origin.x + other.size.x && this.origin.x + this.size.x > other.origin.x;
        const isInY = this.origin.y < other.origin.y + other.size.y && this.origin.y + this.size.y > other.origin.y;
        const v = isInX && isInY;
        return v;
    }
}

const DIST = 4;

export class PhysicsWorld {
    private static memory: Collider[] = [];

    static Gen(tree: ChunkTree, from: Vec2) {
        this.CheckCell(tree, from);
    }

    private static CheckCell(tree: ChunkTree, location: Vec2) {
        const v = tree.locationToChunkBlock(location);
        for (let y = 0; y < DIST; y++) {
            for (let x = 0; x < DIST; x++) {
                const offsetPos = new Vec2(x - DIST / 2, y - DIST / 2);
                const newLocation = location.add(offsetPos.multiply(Tileset.tileSize));

                const loc = v.blockLocation.add(offsetPos).add(tree.target.multiply(Chunk.chunkSize));

                PhysicsWorld.Add(new Collider(loc, new Vec2(1,1), tree.getBlockAt(newLocation) != BlockType.AIR));
            }
        }
    }

    private static Add(collider: Collider) {
        this.memory.push(collider);
    }

    static IsSolid(collider: Collider): boolean {
        this.lastCollider = collider;
        // console.log(collider.origin);
        for (const c of this.memory) {
            if (c.exists) {
                if (collider.overlaps(c)) {
                    return true;
                }
            }
        }
        return false;
    }

    static ClearAll() {
        this.memory = [];
    }

    static lastCollider: Collider = null;
    static DebugDraw(ctx: CanvasRenderingContext2D, tree: ChunkTree) {
        const offset = tree.target
            .multiply(Chunk.chunkSize)
            .multiply(Tileset.tileSize);
        for (const c of this.memory) {
            // if (c.exists) {
            ctx.strokeStyle = c.exists ? "#FFFFFF" : "#0000FF";
            ctx.strokeRect(
                (c.origin.x * Tileset.tileSize.x) - offset.x,
                (c.origin.y * Tileset.tileSize.y) - offset.y,
                c.size.x * Tileset.tileSize.x, c.size.y * Tileset.tileSize.y);
            // }
        }

        if (this.lastCollider) {
            ctx.strokeStyle = "#00FF00";
            ctx.strokeRect(
                (this.lastCollider.origin.x * Tileset.tileSize.x) - offset.x,
                (this.lastCollider.origin.y * Tileset.tileSize.y) - offset.y,
                this.lastCollider.size.x * Tileset.tileSize.x, this.lastCollider.size.y * Tileset.tileSize.y);
        }
    }
}